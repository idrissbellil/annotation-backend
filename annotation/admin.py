from django.contrib import admin

from .models import Label, Image, AnnotationJob, Shape, Point

admin.site.register(Label)
admin.site.register(Image)
admin.site.register(AnnotationJob)
admin.site.register(Shape)
admin.site.register(Point)

