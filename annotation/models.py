from django.db import models

SHAPES = [
        ('RE', 'rectangle'),
        ('PG', 'polygone'),
        ('PL', 'polyline'),
        ('PT', 'point')
        ]

class AnnotationJob(models.Model):
    title = models.CharField(max_length=300)
    storage_server = models.CharField(max_length=300, default='/')
    create_time = models.DateTimeField('date created', auto_now_add=True)
    update_time = models.DateTimeField('date updated', auto_now=True)

    def __str__(self):
        return self.title


class Image(models.Model):
    uri = models.CharField(max_length=300)
    width = models.IntegerField(default=0)
    height = models.IntegerField(default=0)
    job = models.ForeignKey(AnnotationJob, on_delete=models.CASCADE)

    def __str__(self):
        return self.uri


class Label(models.Model):
    job = models.ForeignKey(AnnotationJob, on_delete=models.CASCADE)
    label_text = models.CharField(max_length=300)
    update_date = models.DateTimeField('date updated', auto_now=True)

    def __str__(self):
        return self.label_text

class Shape(models.Model):
    label = models.ForeignKey(Label, on_delete=models.CASCADE)
    update_date = models.DateTimeField('date updated', auto_now=True)
    image = models.ForeignKey(Image, on_delete=models.CASCADE,\
        related_name='shapes')
    shape = models.CharField(choices=SHAPES, default='RE', max_length=100)

    def __str__(self):
        return self.image.uri + '_' + self.label.label_text

class Point(models.Model):
    shape = models.ForeignKey(Shape,\
        related_name= 'points', on_delete=models.CASCADE)
    x = models.IntegerField()
    y = models.IntegerField()

    def __str__(self):
        return "({}, {})".format(self.x, self.y)