# App Usage

A docker compose file is provided, that can be used as it is for usage purposes.
For this, Docker and docker-compose are the only requirements. Then run the run the
service as:

```bash
docker-compose up
```

For development purposes the requirements can be installed, then run as:

```bash
python manage.py runserver 0:8000
```

# API

The API structure can be checked at [API](API.md).
