from django.urls import path, include
from rest_framework.routers import DefaultRouter
from . import views
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework.schemas import get_schema_view

schema_view = get_schema_view(title='Annotation API')

router = DefaultRouter()
router.register('job', views.JobViewSet, basename='job')
router.register('image', views.ImageViewSet)
router.register('label', views.LabelViewSet)
router.register('shape', views.ShapeViewSet)
router.register('point', views.PointViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('schema/', schema_view),
]
