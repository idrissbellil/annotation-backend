from rest_framework import generics
from rest_framework.views import APIView
from django.http import Http404
from rest_framework.response import Response
from rest_framework.decorators import api_view, action
from rest_framework.reverse import reverse
from rest_framework import status, viewsets
from . import models
from . import serializers

class JobViewSet(viewsets.ModelViewSet):
    """
    The Annotation Job related operations.
    """
    queryset = models.AnnotationJob.objects.all()
    serializer_class = serializers.JobSerializer

    # TODO: Combine the two request into one
    @action(detail=True)
    def images(self, request, pk=None, format=None):
        images = models.Image.objects.filter(job=pk)
        serializer = serializers.ImageSerializer(images, many=True,
                context={'request': request})
        return Response(serializer.data)

    @action(detail=True)
    def labels(self, request, pk=None, format=None):
        labels = models.Label.objects.filter(job=pk)
        serializer = serializers.LabelSerializer(labels, many=True,
                context={'request': request})
        return Response(serializer.data)

class ImageViewSet(viewsets.ModelViewSet):
    queryset = models.Image.objects.all()
    serializer_class = serializers.ImageSerializer

class LabelViewSet(viewsets.ModelViewSet):
    queryset = models.Label.objects.all()
    serializer_class = serializers.LabelSerializer

class ShapeViewSet(viewsets.ModelViewSet):
    queryset = models.Shape.objects.all()
    serializer_class = serializers.ShapeSerializer

class PointViewSet(viewsets.ModelViewSet):
    queryset = models.Point.objects.all()
    serializer_class = serializers.PointSerializer
