FROM python:3.7
ENV PYTHONUNBUFFERED 1
RUN mkdir /saf
WORKDIR /saf
COPY requirements.txt /saf/
RUN pip install -r requirements.txt
COPY . /saf/
