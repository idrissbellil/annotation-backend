from rest_framework.decorators import api_view
from rest_framework.response import Response
from matplotlib import pyplot as plt
from skimage.transform import resize
import skimage.color
import numpy as np
import os
import urllib

import tensorflow.compat.v1 as tf
tf.disable_v2_behavior()


sess, graph, labels = None, None, None


def restore_model(path, model_name):
    global sess, graph, labels
    sess = tf.Session()
    try:
        saver = tf.train.import_meta_graph(
            os.path.join(path, model_name))
        saver.restore(sess, tf.train.latest_checkpoint(path))
    except OSError:
        sess, graph, labels = None, None, None
        return
    graph = tf.get_default_graph()
    labels = ['SLICY', 'ZSU_23_4', '2S1', 'BRDM_2']


def compute_suggestions(sess, graph, labels, imgs):
    placeholder = graph.get_tensor_by_name('Placeholder:0')
    prediction = graph.get_tensor_by_name('Softmax:0')

    num_of_imgs = imgs.shape[0]
    num_reps = (num_of_imgs // 16 + (num_of_imgs % 16 and 1))
    upper_16_multiple = num_reps * 16

    # prepare them like [n x 16, 54, 54, 1]
    in_d = np.zeros((upper_16_multiple, 54, 54, 1))
    in_d[:num_of_imgs, :, :, :] = imgs

    predictions = []
    for iii in range(num_reps):
        predictions.extend(
            sess.run(
                prediction,
                feed_dict={placeholder: in_d[iii*16:(iii+1)*16, :, :]}
                ))

    predictions = np.array(predictions)

    return predictions[:num_of_imgs]


restore_model('/home/idriss/MSTAR/saved_models/', 'MSTAR_cla.ckpt.meta')


@api_view(['POST'])
def get_suggestions(request):
    """
    {
        "image_uri": "http://localhost:5000/img_1.png",
        "x": [100, 200],
        "y": [100, 200],
        "labels": ["SLICY", "ZSU_23_4", "2S1", "BRDM_2"]
    }
    """
    required_keys = set(['image_uri', 'x', 'y', 'labels'])
    data = request.data
    assert set(data.keys()).issuperset(required_keys),\
        '{} are not all present in the query.'.format(required_keys)
    x1 = int(min(data['x'][0], data['x'][1]))
    x2 = int(max(data['x'][0], data['x'][1]))
    y1 = int(min(data['y'][0], data['y'][1]))
    y2 = int(max(data['y'][0], data['y'][1]))
    global sess, graph, labels
    if not (sess and graph and labels):
        return Response({"message": "Received", "data": []})
    img_file = urllib.request.urlopen(data['image_uri'])
    img_format = data['image_uri'].rsplit('.', 1)[1]
    img = plt.imread(img_file, format=img_format)
    img = skimage.color.rgb2gray(img)
    img = img[x1:x2, y1:y2]
    img = resize(img, (54, 54), mode='constant')
    img = img.reshape((1, 54, 54, 1))
    scores = compute_suggestions(sess, graph, labels, img)
    score_labels_list = sorted(list(zip(scores[0], labels)), reverse=True)
    new_labels = [b for a, b in score_labels_list]
    return Response({"message": "Received", "data": new_labels})


@api_view(['POST'])
def set_model(request):
    # TODO: set a predefined model
    return Response({"message": "Not implemented yet"})


@api_view(['POST'])
def train_a_model(request):
    # TODO: train a new model using existing annotations
    return Response({"message": "Not implemented yet"})
