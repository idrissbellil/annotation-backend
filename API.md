# Annotation Back-end Django part

## `annotation` API organization

The `coreapi` output that gives the rest-API organization is as follows.

```json
annotation: {
        image: {
            list()
            create(uri, [width], [height])
            read(id)
            update(id, uri, [width], [height])
            partial_update(id, [uri], [width], [height])
            delete(id)
            shapes(id)
        }
        job: {
            list()
            create(title, [storage_server])
            read(id)
            update(id, title, [storage_server])
            partial_update(id, [title], [storage_server])
            delete(id)
            images(id)
            labels(id)
        }
        label: {
            list()
            create(label_text)
            read(id)
            update(id, label_text)
            partial_update(id, [label_text])
            delete(id)
        }
        point: {
            list()
            create(x, y)
            read(id)
            update(id, x, y)
            partial_update(id, [x], [y])
            delete(id)
        }
        shape: {
            list()
            create(label)
            read(id)
            update(id, label)
            partial_update(id, [label])
            delete(id)
            points(id)
        }
    }
    users: {
        list()
        create(username, [email], [is_staff])
        read(id)
        update(id, username, [email], [is_staff])
        partial_update(id, [username], [email], [is_staff])
        delete(id)
    }
```

## `suggestions` API data

```json
{
        "image_uri": "http://localhost:3000/human.jpg",
        "x": [
            1,
            2,
            3,
            4
        ],
        "y": [
            10,
            20,
            30,
            40
        ],
        "labels": [
            "atos",
            "portos",
            "aramis",
            "mousquetaire"
        ]
}
```
