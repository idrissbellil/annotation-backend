from rest_framework import serializers
from . import models

class LabelSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(
            view_name='label-detail')
    job = serializers.PrimaryKeyRelatedField(
                      queryset=models.AnnotationJob.objects.all())
    class Meta:
        model = models.Label
        fields = ('url','id', 'label_text', 'update_date', 'job')

class PointSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(
            view_name='point-detail')
    shape = serializers.PrimaryKeyRelatedField(
                      queryset=models.Shape.objects.all())
    class Meta:
        model = models.Point
        fields = ('id', 'url', 'x', 'y', 'shape')

class ShapeSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(
            view_name='shape-detail')
    image = serializers.PrimaryKeyRelatedField(
                      queryset=models.Image.objects.all())
    points = PointSerializer(many=True, read_only=True)
    label = serializers.PrimaryKeyRelatedField(
                      queryset=models.Label.objects.all())
    class Meta:
        model = models.Shape
        fields = ('url', 'id', 'label', 'update_date', 'points', 'image')

class ImageSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(
            view_name='image-detail')
    job = serializers.PrimaryKeyRelatedField(
                      queryset=models.AnnotationJob.objects.all())
    shapes = ShapeSerializer(many=True, read_only=True)
    class Meta:
        model = models.Image
        fields = ('url', 'id', 'uri', 'width', 'height', 'shapes', 'job')

class JobSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(
            view_name='job-detail')
    images = serializers.HyperlinkedIdentityField(view_name='job-images')
    labels = serializers.HyperlinkedIdentityField(view_name='job-labels')

    class Meta:
        model = models.AnnotationJob
        fields = ('id','url', 'title', 'create_time', 'update_time',
                'storage_server', 'labels', 'images')
